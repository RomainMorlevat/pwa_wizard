import React from 'react'

const BasicInfo = ({ nextStep }) => {
  return (
    <div>
      <input type='text' placeholder='Prénom' />
      <button onClick={nextStep}>Continue</button>
    </div>
  )
}

export default BasicInfo
